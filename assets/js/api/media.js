import {http} from './helper'
import {API_MEDIA_URL} from '@/assets/js/commons/constants'
export default {
  uploadPhoto (token, info) {
    return new Promise((resolve, reject) => {
      http(token).post(`${API_MEDIA_URL}/media/api/photo`, info).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getListMedias (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`${API_MEDIA_URL}/media/api/photo`, {params: params}).then(response => {
        resolve(response.data)
      }).catch(error => {
        reject(error)
      })
    }) 
  }
}