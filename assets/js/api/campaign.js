import {http} from './helper'

export default {
  getAllCampaigns (token, params) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/campaign`, {params: params}).then(rs => {
      http(token).get(`/product/api/campaign`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignV2ById (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/campaign/GetCampaignV2ById/${id}`).then(rs => {
      http(token).get(`/product/api/campaign/GetCampaignV2ById/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignById (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/campaign/${id}`).then(rs => {
      http(token).get(`/product/api/campaign/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createCampaign (token, data) {
    return new Promise((resolve, reject) => {
      // http(token).post(`/api/campaign`, data).then(rs => {
      http(token).post(`/product/api/campaign`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  activeCampaign (token, id){
    return new Promise((resolve, reject) => {
      // http(token).put(`/api/campaign/active/${id}`).then(rs => {
      http(token).put(`/product/api/campaign/active/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateCampaign (token, id, data){
    return new Promise((resolve, reject) => {
      // http(token).put(`/api/campaign/update/${id}`, data).then(rs => {
      http(token).put(`/product/api/campaign/update/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteCampaign (token, id){
    return new Promise((resolve, reject) => {
      // http(token).delete(`/api/campaign/${id}`).then(rs => {
      http(token).delete(`/product/api/campaign/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getAllCampaignDetails (token, params) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/CampaignDetail`, {params: params}).then(rs => {
      http(token).get(`/product/api/CampaignDetail`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignDetailById (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/CampaignDetail`, {params: params}).then(rs => {
      http(token).get(`/product/api/CampaignDetail/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignDetailBranch (token, slug) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/CampaignDetail`, {params: params}).then(rs => {
      http(token).get(`/product/api/CampaignDetail/GetSameBranchs?slug=${slug}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteCampaignDetails (token, id){
    return new Promise((resolve, reject) => {
      // http(token).delete(`/api/CampaignDetail/${id}`).then(rs => {
      http(token).delete(`/product/api/CampaignDetail/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createCampaignDetail (token, data) {
    return new Promise((resolve, reject) => {
      // http(token).post(`http://localhost:50608/api/CampaignDetail`, data).then(rs => {
      http(token).post(`/product/api/CampaignDetail`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateCampaignDetail (token, id, data) {
    return new Promise((resolve, reject) => {
      // http(token).put(`http://localhost:50608/api/CampaignDetail/${id}`, data).then(rs => {
      http(token).put(`/product/api/CampaignDetail/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}