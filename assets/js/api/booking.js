import {http} from './helper'

export default {
  getAllBookings (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/product/api/DiscountCode`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateBookingStatus (token, id, status) {
    return new Promise((resolve, reject) => {
      http(token).put(`/product/api/DiscountCode/${id}?status=${status}` ).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}