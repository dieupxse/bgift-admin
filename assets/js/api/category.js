import {http} from './helper'

export default {
  getAllCategory (token, params) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/category`, {params: params}).then(rs => {
      http(token).get(`/product/api/category`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCategoryById (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/category/getcategorybyid/${id}`).then(rs => {
      http(token).get(`/product/api/category/getcategorybyid/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createdCategory (token, data) {
    return new Promise((resolve, reject) => {
      // http(token).post(`/api/category`, data).then(rs => {
      http(token).post(`/product/api/category`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateCategory (token, id, data) {
    return new Promise((resolve, reject) => {
      // http(token).put(`/api/category/${id}`, data).then(rs => {
      http(token).put(`/product/api/category/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteCategory (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).delete(`/api/category/${id}`).then(rs => {
      http(token).delete(`/product/api/category/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}