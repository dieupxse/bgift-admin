import {http} from './helper'

export default {
  getAllProviders (token, params) {
    return new Promise ((resolve, reject) => {
      // http(token).get(`/api/branch/`, {params: params}).then(rs => {
      http(token).get(`/product/api/branch/`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getProviderById  (token, id){
    return new Promise ((resolve, reject) => {
      // http(token).get(`/api/branch/GetBranchById/${id}`).then(rs => {
      http(token).get(`/product/api/branch/GetBranchById/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createProvider (token, data) {
    return new Promise ((resolve, reject) => {
      // http(token).post(`/api/branch/`, data).then(rs => {
      http(token).post(`/product/api/branch/`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateProvider (token, id, data) {
    return new Promise ((resolve, reject) => {
      // http(token).put(`/api/branch/${id}`, data).then(rs => {
      http(token).put(`/product/api/branch/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteProvider (token, id) {
    return new Promise ((resolve, reject) => {
      // http(token).delete(`/api/branch/${id}`).then(rs => {
      http(token).delete(`/product/api/branch/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}