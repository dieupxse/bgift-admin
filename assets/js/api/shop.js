import {http} from './helper'

export default {
  getAllShops (token, params) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/shop`, {params: params}).then(rs => {
      http(token).get(`/product/api/shop`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getShopById (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).get(`/api/shop/getshopbyid/${id}`).then(rs => {
      http(token).get(`/product/api/shop/getshopbyid/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createShop (token, data) {
    return new Promise((resolve, reject) => {
      // http(token).post(`/api/shop`, data).then(rs => {
      http(token).post(`/product/api/shop`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateShop (token, id, data) {
    return new Promise((resolve, reject) => {
      // http(token).put(`/api/shop/${id}`, data).then(rs => {
      http(token).put(`/product/api/shop/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteShop (token, id) {
    return new Promise((resolve, reject) => {
      // http(token).delete(`/api/shop/${id}`).then(rs => {
      http(token).delete(`/product/api/shop/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}