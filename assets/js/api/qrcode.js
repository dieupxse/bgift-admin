import {http} from './helper'
export default {
  getQrCode(token, keyword="", page=1, rowPerPage=20) {
    return new Promise((resolve, reject) => {
        http(token).get(`/product/api/LinkToQRCode`, {
            params: {
                keyword,
                page,
                rowPerPage
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getQrCodeById(token, id) {
    return new Promise((resolve, reject) => {
        http(token).get(`/product/api/LinkToQRCode/GetLinkById/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createQrCode(token, data) {
    return new Promise((resolve, reject) => {
        http(token).post(`/product/api/LinkToQRCode`, data).then(response => {
        // http(token).post(`${url}/api/grouptext`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateQrCode(token,id,data) {
    return new Promise((resolve, reject) => {
        http(token).put(`/product/api/LinkToQRCode/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteQrCode(token,id) {
    return new Promise((resolve, reject) => {
        http(token).delete(`/product/api/LinkToQRCode/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}
