import {http} from './helper'
import {API_AUTHEN_URL} from '@/assets/js/commons/constants'
export default {
  authenticate (username,password,token) {
    return new Promise((resolve, reject) => {
      http(token).post(`${API_AUTHEN_URL}/authen/api/authentication`, {
          Username: username,
          Password: password
      })
      .then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
