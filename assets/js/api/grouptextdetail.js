import {http} from './helper'
var url ='http://localhost:61709'
export default {
  getGroupTextDetail(token, keyword="", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/grouptext/detail`, {
        // http(token).get(`${url}/api/grouptext/detail`, {
            params: {
                keyword,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextDetailByGroupId(token, groupid) {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/grouptext/detail/group/${groupid}`).then(response => {
        // http(token).get(`${url}/api/grouptext/detail/group/${groupid}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextDetailById(token, id) {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/grouptext/detail/${id}`).then(response => {
        // http(token).get(`${url}/api/grouptext/detail/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createGroupTextDetail(token, data) {
    return new Promise((resolve, reject) => {
        // http(token).post(`${url}/api/grouptext/detail`, data).then(response => {
        http(token).post(`/config/api/grouptext/detail`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateGroupTextDetail(token,id,data) {
    return new Promise((resolve, reject) => {
        http(token).put(`/config/api/grouptext/detail/${id}`, data).then(response => {
        // http(token).put(`${url}/api/grouptext/detail/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteGroupTextDetail(token,id) {
    return new Promise((resolve, reject) => {
        http(token).delete(`/config/api/grouptext/detail/${id}`).then(response => {
        // http(token).delete(`${url}/api/grouptext/detail/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}