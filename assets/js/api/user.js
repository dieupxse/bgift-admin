import {http, axios} from './helper'
import {API_AUTHEN_URL} from '@/assets/js/commons/constants'
export default {
  getCurrentUser (token) {
    return new Promise((resolve, reject) => {
      http(token).get(`${API_AUTHEN_URL}/user/api/user/getcurrentuser`)
      .then(response => {
      resolve(response)
      }).catch(err => {
      reject(err)
      })
    })
  },
  getUsers (token, keyword='', page=1, rowPerPage=20,orderby = "CreateDate", order = "desc") {
    return new Promise((resolve, reject) => {
      http(token).get(`${API_AUTHEN_URL}/user/api/user/?page=${page}&rowPerPage=${rowPerPage}&keyword=${keyword}&orderby=${orderby}&order=${order}`)
      .then(response => {
      resolve(response)
      }).catch(err => {
      reject(err)
      })
    })
  },
  getRoles (token){
    return new Promise((resolve, reject) => {
      http(token).get(`${API_AUTHEN_URL}/user/api/user/getlistroles`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getUserById (token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`${API_AUTHEN_URL}/user/api/user/getuserbyid/${id}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
      })
    })
  },
  createUser (token, user) {
    return new Promise((resolve, reject)=>{
      http(token).post(`${API_AUTHEN_URL}/user/api/user/`, user).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateUser (token, userId, user) {
    return new Promise((resolve, reject) => {
      http(token).put(`${API_AUTHEN_URL}/user/api/user/${userId}`, user).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteUser (token, userId) {
    return new Promise((resolve, reject) => {
      http(token).delete(`${API_AUTHEN_URL}/user/api/user/${userId}`).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}