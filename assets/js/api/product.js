import {http} from './helper'

export default {
  getAllProducts (token, params) {
    return new Promise ((resolve, reject) => {
      // http(token).get(`/api/product/`, {params: params}).then(rs => {
      http(token).get(`/product/api/product/`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getProductById (token, id) {
    return new Promise ((resolve, reject) => {
      // http(token).get(`/api/product/${id}`).then(rs => {
      http(token).get(`/product/api/product/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createProduct (token, data) {
    return new Promise ((resolve, reject) => {
      // http(token).post(`/api/product/`, data).then(rs => {
      http(token).post(`/product/api/product/`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateProduct (token, id, data) {
    return new Promise ((resolve, reject) => {
      // http(token).put(`/api/product/${id}`, data).then(rs => {
      http(token).put(`/product/api/product/${id}`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteProduct (token, id) {
    return new Promise ((resolve, reject) => {
      // http(token).delete(`/api/product/${id}`).then(rs => {
      http(token).delete(`/product/api/product/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
}