import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index'
import LoginPage from '@/pages/login'
import CategoryIndex from '@/pages/category/index'
// Shop
import ShopIndex from '@/pages/shop/index'
// import ShopCreate from '@/pages/shop/create'
// Branch
import BranchIndex from '@/pages/branch/index'
import BranchCreate from '@/pages/branch/create'

// Product
import ProductIndex from '@/pages/product/index'
// import ProductCreate from '@/pages/provider/create'

// Campaign
import CampaignIndex from '@/pages/campaign/index'
import CampaignCreate from '@/pages/campaign/create'
import CampaignEdit from '@/pages/campaign/edit'
// Campaign Detail
import CampaignDetailIndex from '@/pages/campaignDetail/index'
import CampaignDetailCreate from '@/pages/campaignDetail/create'
import CampaignDetailEdit from '@/pages/campaignDetail/edit'

// config
import ConfigGroupText from '@/pages/config/grouptext'
import ConfigGroupTextDetail from '@/pages/config/grouptextdetail'

//qrcode
import Qrcode from '@/pages/qrcode'
import QrcodePrint from '@/pages/qrcode/printQrCode'
// booking
import BookingIndex from '@/pages/booking/index'
Vue.use(Router)
export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: Index
      },
      {
        path: '/login',
        component: LoginPage
      },
      {
        path: '/category',
        component: CategoryIndex
      },
      // shop
      {
        path: '/shop',
        component: ShopIndex
      },
      // branch
      {
        path: '/branch',
        component: BranchIndex
      },
      {
        path: '/branch/create',
        component: BranchCreate
      },
      {
        path: '/branch/create/:id',
        component: BranchCreate
      },
      // product
      {
        path: '/product',
        component: ProductIndex
      },
      //campaign
      {
        path: '/campaign',
        component: CampaignIndex
      },
      {
        path: '/campaign/create',
        component: CampaignCreate
      },
      {
        path: '/campaign/create/:id',
        component: CampaignCreate
      },
      {
        path: '/campaign/edit/:id',
        component: CampaignEdit
      },
      {
        path: '/campaign-detail/:id',
        component: CampaignDetailIndex
      },
      {
        path: '/campaign-detail/create/:id',
        component: CampaignDetailCreate
      },
      {
        path: '/campaign-detail/edit/:id',
        component: CampaignDetailEdit
      },
      // config page
      {
        path: '/config/grouptext',
        component: ConfigGroupText
      },
      {
        path: '/config/grouptext/:id',
        component: ConfigGroupTextDetail
      },
      {
        path: '/qrcode',
        component: Qrcode
      },
      // qrcode page
      {
        path: '/qrcode/:id',
        component: QrcodePrint
      },
      {
        path: '/booking',
        component: BookingIndex
      }
    ],

    scrollBehavior (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  })
}
